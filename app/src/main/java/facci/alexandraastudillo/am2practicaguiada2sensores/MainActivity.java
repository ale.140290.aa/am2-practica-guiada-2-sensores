package facci.alexandraastudillo.am2practicaguiada2sensores;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void ActividadSensorAcelerometro(View view) {
        Intent intent = new Intent(this, ActividadSensorAcelerometro.class);
        startActivity(intent);
    }

}